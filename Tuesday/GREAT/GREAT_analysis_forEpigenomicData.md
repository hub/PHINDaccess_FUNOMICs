# Hands-On: ChIPseq functional analysis with GREAT 

GREAT is a tool GREAT for the functional analysis of cis-regulatory regions. Assigns biological meaning to a set of non-coding genomic regions by analyzing the annotations of the nearby genes. It uses an ORA (Over-Representation analyis) approach. It is useful for peaks identified by any epigenomic profiling technique.
http://great.stanford.edu/public/html/

## Data 

ChIP-seq for SUMO2 in synchronized WI-38 fibroblasts at four different cell cycle phases: G1, S, late S and G2/M. 

<details>
  <summary markdown="span">What's the actual input data for the functional analyis?</summary>

Input data for functional analysis are regions (peaks, bed files) showing significant changes in SUMO enrichment among cell cycle (CC) phases, classified in 4 profiles by profile clustering. 
ChIPseq analyis done using hg19.


* [Cluster 1](Tuesday/GREAT/data/SUMO2_HUMAN_CC_SUMO2profileClust_DMccGenes_AllPeaks.1.bed)
* [Cluster 2](Tuesday/GREAT/data/SUMO2_HUMAN_CC_SUMO2profileClust_DMccGenes_AllPeaks.2.bed)
* [Cluster 3](Tuesday/GREAT/data/SUMO2_HUMAN_CC_SUMO2profileClust_DMccGenes_AllPeaks.3.bed)
* [Cluster 4](Tuesday/GREAT/data/SUMO2_HUMAN_CC_SUMO2profileClust_DMccGenes_AllPeaks.4.bed)

</details>


<details>
  <summary markdown="span">Peaks have been called using the hg19 Human reference genome. Why is this information important?</summary>
Because gene annotation, including gene location varies between assemblies.

</details>


## Perfom analysis 

* Go to http://great.stanford.edu/public/html/

* Choose the test and backgroung regions. 
  <details>
    <summary markdown="span">How how you chose the background regions?</summary>

  Depends on your question: Do you want to compare the functional enrichment in comparison to any random region of the region (Whole genome) or, else, in comparison to a given subset of regions that may have a specific genomic context (e.g. all the SUMO peaks that change along the CC). 

  </details>

* Define the association rules. What's the main effect of the the different rules? 

* Repeat the analysis for the 4 clusters.

## Conclude

Use the output to identify if there is any functional difference between the SUMO regions associated to each profile. How do you proceed?
