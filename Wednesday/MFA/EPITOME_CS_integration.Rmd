---
title: "Epigenomic memory of infection: Data integration"
subtitle : "H3K4me2, transcriptome time courses and chromatin state"
author: "Claudia Chica"
date: "`r format(Sys.time(), '%d %B, %Y')`"
output: 
  html_document: 
    keep_md: yes
    number_sections: yes
    smart: no
    toc: yes
    toc_float: yes
editor_options: 
  chunk_output_type: console
---

```{r setup, message = FALSE, warning = FALSE, echo = FALSE}
knitr::opts_chunk$set(message=FALSE,warning=FALSE,echo=FALSE)#, cache=TRUE, cache.lazy = FALSE)
knitr::opts_knit$set(progress = FALSE, verbose = FALSE)

library(ggplot2)
library(plyranges)
library(RColorBrewer)
library(knitr)
library(pheatmap)
library(patchwork)
library(FactoMineR)
library(org.Hs.eg.db)
library(msigdbr)

WDIR="./"
setwd(WDIR)

```

# Methylation profile

```{r loadEPIGresults}
# Load epigenomic counts
mark="H3K4me2"
fileName=paste0("./RData/countNormLength_",mark,".RData")
load(fileName)

# Get list of DM peaks
fileName=paste0("./RData/DM_results_",mark,".RData")
load(fileName)

# Get DM profiles
fileName=paste0("./RData/DMpeaks_",mark,".RData")
load(fileName)
save(DMs,file=fileName)

kable(table(DMs$Kind,DMs$Sign),caption = paste0(mark,": peak profiles"))
```

# Transcriptional profile

```{r loadTOMEresults}
# Expression data normalised and batch corrected 
fileName=paste0("./RData/TOME_RMAandNoBatch.RData")
load(fileName)

# DA results, same object used for the functional analysis
fileName=paste0("./RData/DE_results_Annot.RData")
load(fileName)

# DEGs padjTh=.05 ; lfcTh=0
fileName=paste0("./RData/DEGs.RData")
load(fileName)

kable(table(DEGs$DEG_kind,DEGs$DEG_sign),caption = "Transcriptome: expression profiles")

```

```{r load_peaks2Gene}
# Load Tgene peak - gene links
fileName=("./RData/peaks2GeneTgene_H3K4me2_links.RData")
load(fileName)

```

# Chromatin states

```{r ENCODE_heatmap, fig.height=10, fig.width=8}
ENCODE_heatmap <- function(matSignal,AnnotationRows,AnnotationCols,colsAnnot) {
breaks=seq(-4,4,.1) ; cols=colorRampPalette(c("purple","black","yellow"))(length(breaks)+1)

pheatmap(matSignal, 
         scale = "row", cluster_rows = T, cluster_cols = F,
         clustering_method = "ward.D2",
         show_rownames = F, show_colnames = F,
         annotation_col = AnnotationCols, 
         annotation_row = AnnotationRows, 
         annotation_colors = colsAnnot,
         color = cols, border = NA,
         fontsize = 12)
}
```

```{r ENCODE_chromatinStates}
# Upload matrix with ENCODE intensities 
fileSignal=paste0("./data/ENCODE_mergedPeaks_",mark,".ov")
matSignal <- read.table(file=fileSignal,header=TRUE,row.names = 1,sep="\t")

# Get profiles = chromatin states -> clustering of ENCODE information
# If with other datasets you get an error on the clustering,
# because some distances are 0 -> delete before all 0 rows
# allZero=which(apply(matSignal,1,sum)==0)
# if (length(allZero)>0) matSignal=matSignal[-allZero,]

# This step may take some time (order of minutes)
fileName=paste0("./RData/",mark,"_allPeaks_CS.RData")
if (file.exists(fileName)) {load(fileName)} else {
mtx=t(matSignal)
d=dist(t(scale(mtx)))
hc=hclust(d,method = "ward.D2")
nClust=8
clust=cutree(hc,k=nClust)
save(clust,file=fileName)
}

```

```{r annotation}
# Get column annotation
fileMetadata="./data/ENCODE_metadata_bigWig_foldChangeOverControl.released.txt"
Encode_metadata <- read.table(file=fileMetadata,header=TRUE,sep="\t")

marks=c("H3K4me3", "H3K4me2", "H3K27ac", "H3K4me1", "H3K79me2", "H3K27me3")
AnnotationCols <- data.frame(
  Mark=factor(sub("-human","",Encode_metadata$Experiment.target),levels = marks),
  row.names = Encode_metadata$File.accession)

# Upload row annotation -> annotation per region 
fileName=paste0("./RData/AnnotationRows.RData")
load(fileName)
# Add chromatin state information from clustering
AnnotationRows$CS=factor(clust[row.names(AnnotationRows)],
                         levels=c("1","2","3","4","5","6","7","8"))

# Define annotation colors
colsG=brewer.pal(n = 3, name = "Greys")
colsM=brewer.pal(n = 4, name = "Greens")
colsS=brewer.pal(n = 3, name = "Blues")[2:3]
nClust=8 ; colsC=brewer.pal(n = nClust, name = "Set2")
colsAnnot <- list(Mark=c("H3K4me3"=colsS[1], "H3K4me2"=colsS[2], "H3K27ac"=colsM[1], "H3K4me1"=colsM[2], "H3K79me2"=colsM[3], "H3K27me3"=colsM[4]),
  DM_sign=c("Gain"="#FF0000", "Loss"="#FF000020"),
  DM_kind=c("DM"="#000000", "notDM"="transparent"),
  Localization=c("TSS"=colsG[1], "intraG"=colsG[2], "interG"=colsG[3]),
  CS=c("1"=colsC[1],"2"=colsC[2],"3"=colsC[3],"4"=colsC[4],"5"=colsC[5],"6"=colsC[6],"7"=colsC[7],"8"=colsC[8]))

```

## Plot peaks annotation

```{r ENCODE_counts, fig.height=10, fig.width=8, cache=TRUE}
# Tables
t=table(AnnotationRows$CS)
kable(ceiling(t/sum(t)*100),col.names = c("CS","Freq"))

t1=table(AnnotationRows$Localization)
tt=table(AnnotationRows$CS,AnnotationRows$Localization)
kable(ceiling(t1/sum(t1)*100),col.names = c("Localization","Freq"))
kable(ceiling(tt/as.numeric(t)*100),row.names = T)

# Produce heatmap only on a subset of peaks, to make it faster .... does it look fine?
# Explain
ranIdx=sample(1:dim(matSignal)[1],3e3)
ranPeaks=row.names(matSignal)[ranIdx]
ENCODE_heatmap(matSignal[ranPeaks,],AnnotationRows[ranPeaks,],AnnotationCols,colsAnnot)
#ENCODE_heatmap(matSignal,AnnotationRows,AnnotationCols,colsAnnot)

```

# Multi factorial analysis

Multi factorial analysis (MFA) (Escofier & Pages, Computational Statistics & Data Analysis, 1994) studies several groups of variables (numerical and/or categorical) defined on the same set of individuals. MFA is a factor analysis applied to the array including all groups of variables. Roughly, the behavior of the method is equivalent to PCA (concerning quantitative variables) or to MCA (concerning qualitative variables).

```{r MFA_functions}
#http://juliejosse.com/wp-content/uploads/2019/03/MFA_staf2015.pdf

MFAstats <- function(mfa.res,xlim,ylim) {

# How many components are necessary to describe the total inertia of the dataset
print(kable(mfa.res[["eig"]][1:10,],
            caption = "Eigen values of global analysis"))
# Correlation coeffs
# How much of the stt defined by component C depends on groups(s) G : G -> C
# Fundamental to see if there are structures COMMON to groups -> useful to integrate 
print(kable(mfa.res[["group"]][["correlation"]],caption = "Group correlation with factor"))
# Inertia or group contribution 
# How much of the inertia of group G is explained by component C : C -> G
print(kable(mfa.res[["group"]][["cos2"]],caption = "Group inertia per factor"))
# Group contribution 
plotGrouprepresentation(mfa.res)
# Variable contribution 
plotVarrepresentation(mfa.res)
# Individual contribution
plotIndrepresentation(mfa.res,xlim,ylim)
}

plotGrouprepresentation <- function(mfa.res) {
p1 <- plot.MFA(mfa.res,choix = "group",axes = c(1,2), title = "")
p2 <- plot.MFA(mfa.res,choix = "group",axes = c(1,3), title = "")
p3 <- plot.MFA(mfa.res,choix = "group",axes = c(1,4), title = "")
p4 <- plot.MFA(mfa.res,choix = "group",axes = c(1,5), title = "")
print((p1 | p2) / (p3 | p4)  + plot_annotation(title = 'Group representation'))
}

plotVarrepresentation <- function(mfa.res) {
  title=""
  p1 <- plot.MFA(mfa.res,choix="var", axes=c(1,2),title=title,cex=.7)
  p2 <- plot.MFA(mfa.res,choix="var", axes=c(1,3),title=title,cex=.7)
  p3 <- plot.MFA(mfa.res,choix="var", axes=c(1,4),title=title,cex=.7)
  p4 <- plot.MFA(mfa.res,choix="var", axes=c(1,5),title=title,cex=.7)
  
print((p1 | p2) / (p3 | p4) + plot_layout(guides = "collect") + plot_annotation(title = 'Variable representation'))

}

plotIndrepresentation <- function(mfa.res,xlim,ylim) {
  if (ylim[1]!="" & xlim[1]!="") {
p1 <- plot.MFA(mfa.res,choix = "ind", invisible = "ind", axes = c(1,2), 
               title = "",ylim=ylim,xlim=xlim)
p2 <- plot.MFA(mfa.res,choix = "ind", invisible = "ind", axes = c(1,3),
               title = "",ylim=ylim,xlim=xlim)
p3 <- plot.MFA(mfa.res,choix = "ind", invisible = "ind", axes = c(1,4),
               title = "",ylim=ylim,xlim=xlim)
p4 <- plot.MFA(mfa.res,choix = "ind", invisible = "ind", axes = c(1,5),           
               title = "",ylim=ylim,xlim=xlim) 
} else  
{
p1 <- plot.MFA(mfa.res,choix = "ind", invisible = "ind", axes = c(1,2), title = "")
p2 <- plot.MFA(mfa.res,choix = "ind", invisible = "ind", axes = c(1,3), title = "")
p3 <- plot.MFA(mfa.res,choix = "ind", invisible = "ind", axes = c(1,4), title = "")
p4 <- plot.MFA(mfa.res,choix = "ind", invisible = "ind", axes = c(1,5), title = "")
  
}
print((p1 | p2) / (p3 | p4) + plot_annotation(title = 'Individual representation'))
}

clusterStats <- function(hcpc.res) {
### Cluster description
## By variables: 
# Test if variable (OMIC samples) values distribution per cluster is random or not 
# For quantitative variables
print(kable(hcpc.res[["desc.var"]]$quanti,caption = "Description by quantitative variables"))
# For qualitative variables
print(kable(hcpc.res[["desc.var"]][["category"]],caption = "Description by qualitative variables"))
## By components 
# Test if individuals coordinates distribution per cluster is random or not 
# For quantitative variables
print(kable(hcpc.res[["desc.axes"]][["quanti"]],
            caption = "Description by factors (quantitative variables)"))
# For qualitative variables
print(kable(hcpc.res[["desc.var"]][["test.chi2"]],
            caption = "Description by factors (qualitative variables)"))
## By individuals (genomic regions)
# Nearest individuals to the cluster's centroid
print("Description by individuals (nearest):")
print(hcpc.res[["desc.ind"]]$para)
# Furthest individuals to the cluster's centroid
print("Description by individuals (furthest):")
print(hcpc.res[["desc.ind"]]$dist)
}

```

## Prepare MFA input matrix 

```{r MFA_prepareMtx}
## Qualitative data
DEGs2peaks=merge(DEGs,peaks2GeneTgene_H3K4me2,by.x=1,by.y=2)
QualitativeData=merge(DEGs2peaks[,c(5,1,2,3,4,6,7,8)],AnnotationRows[,c(4,2,3)],by.x=1,by.y=0)
QualitativeData$Distance=log(abs(QualitativeData$Distance)+1,10)

## Prepare methylation: scale normalised counts, 
## add DM inigial and final time points: D7vsUI 
METHOME=merge(t(scale(t(countNormLength_H3K4me2))),
              DM_results$D7vsUI[,1:2],by=0)
METHOME$baseMean=log(METHOME$baseMean,2)
colnames(METHOME)[8:9]=c("H3K4me2_log2baseMean","H3K4me2_D7vsUI_log2FC")

## Prepare transcriptome, log2
TOME=log(TOME+1,2)

## Prepare ENCODE data: get mean intensity per mark and scale
samples=AnnotationCols[colnames(matSignal),]
names(samples)=colnames(matSignal)
ENCODEmean=t(scale(t(
  data.frame(
  H3K4me3=apply(matSignal[,names(samples[samples=="H3K4me2"])],1,function(x) mean((x))),
  H3K4me2=apply(matSignal[,names(samples[samples=="H3K4me2"])],1,function(x) mean((x))),
  H3K4me1=apply(matSignal[,names(samples[samples=="H3K4me1"])],1,function(x) mean((x))),
  H3K27ac=apply(matSignal[,names(samples[samples=="H3K27ac"])],1,function(x) mean((x))),
  H3K79me2=apply(matSignal[,names(samples[samples=="H3K79me2"])],1,function(x) mean((x))),
  H3K27me3=apply(matSignal[,names(samples[samples=="H3K27me3"])],1,function(x) mean((x))),
  row.names = row.names(matSignal))
)))

## Merge qualitative data/annotations, methylation, ENCODE and transcriptome signal
EPITOME=merge(QualitativeData,METHOME,by=1)
EPITOME=merge(EPITOME,ENCODEmean,by.x=1,by.y=0)
EPITOME=merge(EPITOME,TOME,by.x=2,by.y=0)
#row.names(EPITOME)=paste0(EPITOME$peakID,"_",EPITOME$ENTREZID)
# Reorder columns .... just for order :-)
EPITOME=EPITOME[,c(2,1,3,6:11,4,5,12:36)]


## Select peak-gene association that contain a DM or a DEG
mfa.EPITOME=subset(EPITOME,EPITOME$DM_kind!="notDM" | EPITOME$DEG_kind!="notSpecific")

#fileName=paste0("./RData/mfa.EPITOME.RData")
#save(EPITOME,file=fileName)
#load(fileName)

```

MFA is performed on the `r dim(mfa.EPITOME)[1]` peak-gene associations that include a DMR or a DEG. This is the distribution of quantitative and qualitative group variables.

```{r MFA_distribution}
## Check MFA matrix distribution
boxplot(mfa.EPITOME[,c(26:36,18:19,4,5,20:25)],las=2,pch=19,cex=.5,
        ylab="Scaled values by group",cex.axis=.5,
        col=c(rep(palette()[8],11),rep(palette()[6],2),
              rep(palette()[3],1),rep(palette()[4],1),rep(palette()[5],6)),
        main="Distribution of quantitative variables for MFA")

tmp=unique(mfa.EPITOME[,c(3,10,11)])
kable(table(tmp[,2:3]),caption = "DEGs for MFA")
tmp=unique(mfa.EPITOME[,c(2,8,9)])
kable(table(tmp[,2:3]),caption = "DMRs for MFA")
```

## Perfom MFA and components description

```{r MFA, fig.height=10, fig.width=10, cache=TRUE}
# For the sake of speed select only DM peaks plus 500 random peaks 
idx=unique(c(which(mfa.EPITOME$DM_kind=="DM"),sample(1:dim(mfa.EPITOME)[1],500)))

# Try different MFA settings
colInds=c(26:36,18:19,4,5,20:25,7,10:11,8:9)
fileOut=paste0(WDIR,"./RData/mfa.res_TOMEMETHDISTCORREPIGDEGDMR_CS_filtered.RData")
if (file.exists(fileOut)) {load(fileOut)} else {
  mfa.res_TOMEMETHDISTCORREPIGDEGDMR_CS  <- MFA(mfa.EPITOME[idx,colInds], ncp=5, 
    group=c(11,2,1,1,6,1,2,2), type=c(rep("c",5),rep("n",3)),
    name.group=c("TOME","METH","DIST","CORR","EPIG","CS","DEG","DMR"),num.group.sup=c(6),
    graph = FALSE)
save(mfa.res_TOMEMETHDISTCORREPIGDEGDMR_CS,file=fileOut)
}

colInds=c(26:36,18:19,4,5,20:25,10:11,8:9)
fileOut=paste0(WDIR,"./RData/mfa.res_TOMEMETHDISTCORREPIGDEGDMR.RData")
if (file.exists(fileOut)) {load(fileOut)} else {
  mfa.res_TOMEMETHDISTCORREPIGDEGDMR  <- MFA(mfa.EPITOME[idx,colInds], ncp=5, 
    group=c(11,2,1,1,6,2,2), type=c(rep("c",5),rep("n",2)),
    name.group=c("TOME","METH","DIST","CORR","EPIG","DEG","DMR"),graph = FALSE)
save(mfa.res_TOMEMETHDISTCORREPIGDEGDMR,file=fileOut)
}


#### MFA stats
mfa.res=mfa.res_TOMEMETHDISTCORREPIGDEGDMR
xlim=c(-2,2) ; ylim=c(-2,2)
MFAstats(mfa.res,xlim,ylim)

```

# Hierarchical clustering on MFA components

Clustering is performed on the first 5 components obtained with the MFA.

```{r HCPCclustering, fig.height=10, fig.width=10, cache=TRUE}

#### Clustering
nClust=10 #; maxClustSize=1400
hcpc.res <- HCPC(mfa.res, nb.clust=nClust, conso=0, min=3, max=10,
                 metric = "euclidean", method="ward",graph = TRUE)#,nb.par = maxClustSize)

# Cluster description in term of qualitative variables
lapply(hcpc.res[["desc.var"]][["category"]],function(x) {kable(subset(x,x[,"v.test"]>5))})

lapply(hcpc.res[["desc.var"]][["category"]],function(x) {kable(subset(x,x[,"v.test"]>5))})

```

# References

```{r}
sessionInfo()
```

