# Hands-On: Exploring PATRIC 



## Organism: Mycobacterium tuberculosis (strain erdmann)

<!-- <img src="images/ATAC-seq_PE.png" width="700" align="center" > -->

## Perfom search 

Go to https://www.patricbrc.org/

* Search for the genome
        choose 653626.4
* Check the overview tab, look for:
```
Num. of prots
Num. of annotated prots with an associated  Gene Ontology 
Num. of enzymes
```
                
* Check the features tab
and add the GO terms
* Check all the entries and
download file (.xls)
* Put the file in the data directory




