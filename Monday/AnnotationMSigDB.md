# Hands-On: Advanced annotation with MSigDB

Go to 
https://www.gsea-msigdb.org/gsea/msigdb/

Note: you need to register in the MSigDB to be able to use it.

## Organism: Human

<!-- <img src="images/ATAC-seq_PE.png" width="700" align="center" > -->

## Exploring gene families for human

* What  "gene families" families does the MSigDB define ?

* From these genes, which transcription factors act as tumor suppressors ?

## Performing full annotation exploration for human

* Choose a specific colection from Human, and download the correspoding  annotation.

* Produce an excel table with the annotation.

* Explain how many gene sets are present and what type of functions the selected collection has.
Who is the contributor to this collection?


## Exploring which in which pathways is human Tau protein involved in 

* Use the keyword field to type the gene name

* Choose as collection "canonical pathways"

* Choose human as organism and select a contributor
