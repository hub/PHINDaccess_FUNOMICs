# PHINDaccess FUNOMICs


## Hands-on programme

| Course    | training        | 
|-----------|--------------------|
| Functional annotation 0: Exploring PATRIC | [Hands-on](Monday/Exploring_PATRIC.md)  
| Functional annotation I: Annotation using Gene Ontology and Kegg | [Hands-on](Monday/practicals_day1_PHINDAccess.Rmd)  
| Functional annotation II: Advanced annotations with MSigDB| [Hands-on](Monday/AnnotationMSigDB.md) 
| Functional analysis: GSEA | [Hands-on](Tuesday/practicals_day2_PHINDAccess.Rmd )  |
| Functional analysis: GREAT | [Hands-on](Tuesday/GREAT/GREAT_analysis_forEpigenomicData.md)  |
| Functional analysis: methylGSA | [Hands-on](Tuesday/methylGSA_Functional_analysis/FuncAnalysis_methylGSA.Rmd)  |
| Single OMICs: Descriptive analysis | [DEMO](Wednesday/Descriptive/Descriptive_analysis.Rmd)  |
| Single OMICs: Discriminative analysis | [Hands-on](Wednesday/SDA/SDA.Rmd) |
| Integration: MFA | [Hands-on](Wednesday/MFA/MFA.Rmd)  |
| Networks 1: PPIs | [Hands-on](Thursday/String_DB_HandsOn_Diabetes.md) |
| Networks 2: PPIs | [Hands-on]( Thursday/StringDB-HandsOn_CANCER.md) |
| Networks 3: Regulatory networks | [Hands-on](Thursday/OmicsNet_practical.md)  |


## Trainers 

- Natalia Pietrosemoli: <natalia.pietrosemoli@pasteur.fr> 
- Claudia Chica: <claudia.chica@pasteur.fr> 
